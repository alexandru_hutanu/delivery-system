<?php
namespace DeliverySystem\ConfigurationBundle\Service;

use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigurationService
 * @package DeliverySystem\ConfigurationBundle\Service
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class ConfigurationService
{
    const CONFIG_YAML_PATH = 'config/config.yml';

    /** @var array */
    private $databaseConfig;

    /** @var string */
    private $dataProvider;

    /**
     * @throws \RuntimeException
     */
    public function __construct()
    {
        $currentDir = dirname(__FILE__);
        $pathToYml  = $currentDir .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . self::CONFIG_YAML_PATH;
        
        if (!file_exists($pathToYml)) {
            throw new \RuntimeException(sprintf('Cannot find file %s', $pathToYml));
        }

        $yamlConfig           = file_get_contents($pathToYml);
        $configuration        = Yaml::parse($yamlConfig);
        $this->databaseConfig = $configuration['databaseConfiguration'];
        $this->dataProvider   = $configuration['dataProvider'];
    }

    /**
     * @return array
     */
    public function getDatabaseConfiguration()
    {
        return $this->databaseConfig;
    }

    /**
     * @return string
     */
    public function getDataProvider()
    {
        return $this->dataProvider;
    }
}