<?php
namespace DeliverySystem\TrackingBundle\Test;

use Silex\WebTestCase;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use DeliverySystem\TrackingBundle\Controller\ReadController;
use DeliverySystem\TrackingBundle\Service\EstimateService;

/**
 * Class ReadControllerTest
 * @package DeliverySystem\TrackingBundle\Test
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class ReadControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testReadControllerPositive()
    {
        $estimate = '2016-10-10 21:00:00';

        $readController = new ReadController(
            $this->getRequestMock(),
            $this->getEstimateServiceMockWithEstimate($estimate)
        );
        $response = $readController->indexJsonAction();

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['delivery_estimate']);
        $this->assertEquals($estimate, $content['delivery_estimate']);
    }

    public function testReadControllerNegative()
    {
        $readController = new ReadController(
            $this->getRequestMock(),
            $this->getEstimateServiceMockWithEstimate(null)
        );
        $response = $readController->indexJsonAction();

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['error']);
    }

    private function getRequestMock()
    {
        $stub = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $stub->method('get')
             ->willReturn('123456789');

        return $stub;
    }

    private function getEstimateServiceMockWithEstimate($estimate)
    {
        $stub = $this->getEstimateServiceMock();

        $stub->method('getDeliveryEstimateForTrackingCode')
            ->willReturn($estimate);

        return $stub;
    }

    private function getEstimateServiceMock()
    {
        return $this->getMockBuilder(EstimateService::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();
    }
}