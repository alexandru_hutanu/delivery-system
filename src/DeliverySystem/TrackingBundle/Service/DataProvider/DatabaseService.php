<?php
namespace DeliverySystem\TrackingBundle\Service\DataProvider;

use DeliverySystem\TrackingBundle\Service\DataProvider\DataProviderInterface;

/**
 * Class DatabaseService
 * @package DeliverySystem\TrackingBundle\Service\DataProvider
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class DatabaseService implements DataProviderInterface
{
    /** @var \PDO $databaseConnection */
    private $databaseConnection;

    /**
     * DatabaseService constructor.
     * @param $databaseConfiguration
     * @throws \Exception
     */
    public function __construct($databaseConfiguration)
    {
        if (!is_array($databaseConfiguration)) {
            throw new \Exception('Improper database configuration. Please check config/config.yml and try again');
        }

        foreach ($databaseConfiguration as $config) {
            if (empty($config)) {
                throw new \Exception(
                    'Missing database configuration. Please check config/config.yml and try again'
                );
            }
        }

        $this->databaseConnection = new \PDO(
            "mysql:host" . $databaseConfiguration['host'] .
            ";port=" . $databaseConfiguration['port'] .
            ";dbname=" . $databaseConfiguration['dbname'],
            $databaseConfiguration['username'],
            $databaseConfiguration['password']
        );
    }

    /**
     * @param $trackingCode
     * @return null
     */
    public function getEstimateByTrackingCode($trackingCode)
    {
        $sql = "
        SELECT
        `dd`.`estimated_delivery`
        FROM
          `delivery_system`.`delivery_data` `dd`
        WHERE `dd`.`tracking_code` = :trackingCode
        ";

        $statement = $this->databaseConnection->prepare($sql);

        $statement->bindParam('trackingCode', $trackingCode);

        $statement->execute();

        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($data) && isset($data[0]['estimated_delivery'])) {
            return $data[0]['estimated_delivery'];
        }

        return null;
    }
}
