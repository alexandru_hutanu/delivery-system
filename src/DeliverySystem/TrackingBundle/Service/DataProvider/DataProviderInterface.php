<?php
namespace DeliverySystem\TrackingBundle\Service\DataProvider;

/**
 * Interface DataProviderInterface
 * @package DeliverySystem\TrackingBundle\Service\DataProvider
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
interface DataProviderInterface
{
    public function getEstimateByTrackingCode($trackingCode);
}