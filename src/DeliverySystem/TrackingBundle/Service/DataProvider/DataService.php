<?php
namespace DeliverySystem\TrackingBundle\Service\DataProvider;

use DeliverySystem\TrackingBundle\Service\DataProvider\DataProviderInterface;

/**
 * Class DataService
 * @package DeliverySystem\TrackingBundle\Service\DataProvider
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class DataService implements DataProviderInterface
{
    /** @var string */
    const CSV_FILE_ROOT = 'data/delivery_data.csv';

    /** @var $file */
    private $file;

    /**
     * DataService constructor.
     */
    public function __construct()
    {
        $currentDir       = dirname(__FILE__);
        $pathToCsv  = $currentDir .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . self::CSV_FILE_ROOT;

        if (!file_exists($pathToCsv)) {
            throw new \RuntimeException(sprintf('Cannot find file %s', $pathToCsv));
        }

        $this->file = fopen($pathToCsv, 'r');
    }

    /**
     * @param $trackingCode
     * @return null
     */
    public function getEstimateByTrackingCode($trackingCode)
    {
        foreach ($this->getCSV() as $data) {
            list($tracking_code, $estimatedDelivery) = $data;

            if ($tracking_code == $trackingCode) {
                return $estimatedDelivery;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    private function getCSV()
    {
        $data = [];

        while (!feof($this->file)) {
            $data[] = fgetcsv($this->file, 'r');
        }

        return $data;
    }
}