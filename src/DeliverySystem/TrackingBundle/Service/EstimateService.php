<?php
namespace DeliverySystem\TrackingBundle\Service;

use DeliverySystem\TrackingBundle\Service\DataProvider\DataProviderInterface;

/**
 * Class EstimateService
 * @package DeliverySystem\TrackingBundle\Service
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class EstimateService
{
    /** @var DataProviderInterface $dataProvider */
    private $dataProvider;

    /**
     * EstimateService constructor.
     * @param DataProviderInterface $dataProvider
     */
    public function __construct(DataProviderInterface $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param $trackingCode
     * @return mixed
     */
    public function getDeliveryEstimateForTrackingCode($trackingCode)
    {
        return $this->dataProvider->getEstimateByTrackingCode($trackingCode);
    }
}