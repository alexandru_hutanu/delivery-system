<?php
namespace DeliverySystem\TrackingBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use DeliverySystem\TrackingBundle\Service\EstimateService;

/**
 * Class ReadController
 * @package DeliverySystem\TrackingBundle\Controller
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class ReadController
{
    /** @var Request $request */
    private $request;

    /** @var EstimateService $estimateService */
    private $estimateService;

    /**
     * ReadController constructor.
     * @param Request $request
     * @param EstimateService $estimateService
     */
    public function __construct(Request $request, EstimateService $estimateService)
    {
        $this->request         = $request;
        $this->estimateService = $estimateService;
    }

    /**
     * @return JsonResponse
     */
    public function indexJsonAction()
    {
        $trackingCode = $this->request->get('trackingCode');

        try {
            $deliveryEstimate = $this->estimateService->getDeliveryEstimateForTrackingCode($trackingCode);

            $response = [
                'delivery_estimate'  => $deliveryEstimate,
            ];

            if (null === $deliveryEstimate) {
                $response = [
                    'error' => 'Cannot find delivery date for tracking code ' . $trackingCode
                ];
            }
        } catch (\Exception $exc) {
            $response = [
                'error' => $exc->getMessage()
            ];
        }

        return new JsonResponse($response);
    }
}