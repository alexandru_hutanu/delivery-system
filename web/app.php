<?php

require_once __DIR__
    . DIRECTORY_SEPARATOR
    . '..'
    . DIRECTORY_SEPARATOR
    . 'vendor'
    . DIRECTORY_SEPARATOR
    . 'autoload.php'
;

use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use \Symfony\Component\HttpFoundation\Request;
use DeliverySystem\ConfigurationBundle\Service\ConfigurationService;
use DeliverySystem\TrackingBundle\Controller\ReadController;
use DeliverySystem\TrackingBundle\Service\EstimateService;
use DeliverySystem\TrackingBundle\Service\DataProvider\DatabaseService;
use DeliverySystem\TrackingBundle\Service\DataProvider\DataService;

/**
 * App Engine
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */

$app = new Application();
$app['debug'] = true; //enable debug

/**
 * Register services
 */

$configurationService = new ConfigurationService();

//set default to csv
$app['data_provider'] = function () use ($configurationService) {
    return new DataService();
};

if ('sql' == $configurationService->getDataProvider()) {
    $app['data_provider'] = function () use ($configurationService) {
        return new DatabaseService($configurationService->getDatabaseConfiguration());
    };
}

$app['estimate_service'] = function () use ($app) {
    return new EstimateService($app['data_provider']);
};

/**
 * Register controllers
 */

//register Controller as a Service provider
$app->register(new ServiceControllerServiceProvider());

//register Read Controller
$app['read.controller'] = function() use ($app) {
    return new ReadController($app['request_stack']->getCurrentRequest(), $app['estimate_service']);
};

/**
 * Routes & controllers actions mapping
 */

//dummy homepage
$app->get('/', function() use ($app) {
    return 'Dummy Homepage';
});

$app->get('/read/{trackingCode}', "read.controller:indexJsonAction");

$app->run();
