# Delivery System Backend API

## Installation
1. Run composer install
2. Prepare SQL database by run all queries in sql/database_setup.sql
3. Copy config/config.yml.sample to config/config.yml and fill all necessary data as follows:
 1. Param dataProvider should have the value "sql" or "csv" according to the desired storage solution
 2. Inside the dataConfiguration array, params will be completed as follows:
 3. host would have the value of sql host
 4. port will have the value of sql port (e.g. 3306)
 5. dbname will have the value of the database name to connect to (if all queries were run properly at point 2, this should be delivery_data)
 6. username and password will be the username and password for sql
4. The tracking codes and their delivery date can be changed from data/delivery_data.csv for csv storage solution or from inside the INSERT statement from sql query in sql/database_setup.sql
5. Default tracking codes and delivery dates are:

Tracking Code   | Delivery Date
----------------|-----------------------------------
Code: 123456789 | Delivery Date: 2016-10-11 10:00:00
Code: 987654321 | Delivery Date: 2016-11-11 09:00:00
Code: 123454321 | Delivery Date: 2016-12-12 08:00:00

## Backend API Tracking Route: GET /read/{trackingCode}


## Request & Response Example:
`
$ curl -X GET delivery.system/read/123456789
`

`
{"delivery_estimate":"2016-10-11 10:00:00"}
`

## Unit Test Run Example:
`
$ ./vendor/bin/phpunit src/DeliverySystem/TrackingBundle/Test/ReadControllerTest.php
`