CREATE DATABASE `delivery_system`;

CREATE TABLE IF NOT EXISTS `delivery_system`.`delivery_data` (
	  `delivery_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `tracking_code` int(9) unsigned NOT NULL,
	  `estimated_delivery` datetime NOT NULL,
	  PRIMARY KEY (`delivery_data_id`),
	  INDEX `i_tracking_code_estimated_delivery` (`tracking_code`, `estimated_delivery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delivery_system`.`delivery_data`
(`tracking_code`, `estimated_delivery`)
VALUES
('123456789', '2016-10-11 10:00:00'),
('987654321', '2016-11-11 09:00:00'),
('123454321', '2016-12-12 08:00:00');
